﻿class NYPJobOffer extends HTMLElement {
    constructor() {
        super();

        this.component = document.getElementById('nyp-job-offer');
        this.getForm();
    }

    render() {
        const instance = document.importNode(this.component.content, true);
        
    }

    async getForm() {
        const request = await fetch('https://newyorkpizza.recruitee.com/o/content-marketeer-amstelveen/c/new');
        const response = await request.text();

        this.parseForm(response);

        //this.appendChild(applyForm);
    }

    parseForm(response) {
        const domParser = new DOMParser().parseFromString(response, 'text/html');
        const applyForm = domParser.querySelector('form');
        const submitBtn = applyForm.querySelector('input[type=submit]');

        submitBtn.addEventListener('click', this.apply);

        this.appendChild(applyForm);
    }

    async apply() {
        console.log(jQuery);

        const request = await fetch('/wp-json/prodes-recrutee/v1/public/offers/content-marketeer-amstelveen/candidates', {
            method: 'POST'
        });
        const response = await request.json();

        return false;
    }
}

ProdesComponents.register('nyp-job-offer', NYPJobOffer);
﻿/**
 * Just a implementation of web components to make development easier
 * This class is a singleton
 * 
 * Components should be stored like so
 *          - /wp-content/{...}/components/example.component.js
 *          - /wp-content/{...}/components/example.component.html
 * Where `example` is the name of the component. Register that name using the register() function. 
 * 
 * @version 1.0
 * @author  Jordy Boekema
*/
class ProdesComponents {
    constructor() {
        if (ProdesComponents.instance)
            return ProdesComponents.instance;

        this.config = {
            path: '/wp-content/plugins/prodes-recrutee/components/',
            components: {}
        }

        ProdesComponents.instance = this;
    }

    /**
     * Register new component
     * 
     * @param {string}      name
     * @param {callable}    callable
     */
    static async register(name, callable) {
        const ProdesComponents = new this();

        if (ProdesComponents.config.components[name])
            return;

        await ProdesComponents.fetchComponent(name);
        ProdesComponents.config.components[name] = customElements.define(name, callable);
        ProdesComponents.config.components[name] = callable;

        return ProdesComponents;
    }

    /**
     * Unregister a component
     * 
     * @param {any} name
     */
    static unregister(name) {
        return this.components.some((component, index) => {
            if (component.name === name) {
                this.components.slice(0, index);

                return this;
            };
        })
    }

    /**
     * Gets the component HTML
     * 
     * @param {string} name
     */
    async fetchComponent(name) {
        const componentHtml = await fetch(`${this.config.path}/${name}.component.html`);
        const componentHtmlResponse = await componentHtml.text();

        if (componentHtml.status !== 200)
            return;

        document.body.insertAdjacentHTML('beforeend', componentHtmlResponse);
    }
}

new ProdesComponents();
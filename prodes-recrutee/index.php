<?php
/**
 * Plugin Name: Prodes - Recrutee API
 * Plugin URI: https://prodes.nl
 * Description: Recrutee API implementation
 * Version: 1.0.0
 * Author: Jordy Boekema
 **/

if ( ! defined( 'ABSPATH' ) )
{
	exit; // Exit if accessed directly
}

require_once __DIR__ . '/classes/recrutee/offer.class.php';
require_once __DIR__ . '/classes/recrutee/public-api.class.php';
require_once __DIR__ . '/classes/curl/curl.class.php';
require_once __DIR__ . '/classes/shortcode.class.php';
require_once __DIR__ . '/classes/rest-callbacks.class.php';
require_once __DIR__ . '/classes/rest-routes.class.php';
require_once __DIR__ . '/classes/init.class.php';
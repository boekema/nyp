<?php
/**
 * @version v1.0.0
 * @author Jordy Boekema
 */
class Prodes_Recrutee_Shortcode
{
    public function __construct()
    {
        add_shortcode( 'sollicitatie_formulier', array( $this, 'add_job_application_form_shortcode' ) );
    }

    public function add_job_application_form_shortcode( $atts = null )
    {
        $a = shortcode_atts( array(
		    'offer_id' => null
	    ), $atts );

        $offer = Prodes_Recrutee_Offer::instance( $atts['offer_id'] );

        ob_start();
        include dirname( __DIR__ ) . '/templates/job-application-form.template.php';
        $output = ob_get_clean();

        return $output;
    }
}
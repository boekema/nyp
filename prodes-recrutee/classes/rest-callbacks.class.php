<?php
/**
 * @version v1.0.0
 * @author Jordy Boekema
 */
class Prodes_Recrutee_REST_Callbacks
{
    public static function get_offers( WP_REST_Request $request )
    {
        return Prodes_Recrutee_Public_API::offers();
    }

    public static function import_offers( WP_REST_Request $request )
    {
        return Prodes_Recrutee_Init::import_offers_cron();
    }

    public static function apply_for_offer( WP_REST_Request $request )
    {
        return Prodes_Recrutee_Offer::apply( '', '', null, null, $request->get_param('offer_slug') );
    }
}
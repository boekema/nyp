<?php
/**
 * @version v1.0.0
 * @author Jordy Boekema
 */
class Prodes_Recrutee_Init
{
    /**
     * Construct plugin
     */
    public function __construct()
    {
        add_action( 'init', array( $this, 'register_post_type' ), 0 );
        add_action( 'wp_head', array( $this, 'add_assets' ) );

        //add_action( 'init', array( $this, 'import_offers_cron' ), 0 );

        add_filter('use_block_editor_for_post_type', array( $this, 'disable_gutenberg' ), 10, 2);

        new Prodes_Recrutee_Shortcode();
    }

    /**
     * Init front end plugin
     *
     * @since v1.0.0
     */
    public function init()
    {

    }

    public function add_assets()
    {
        wp_enqueue_style( 'prodes-recrutee-bootstrap', '/wp-content/plugins/prodes-recrutee/assets/css/bootstrap.min.css' );
        wp_enqueue_script( 'prodes-recrutee-script', '/wp-content/plugins/prodes-recrutee/assets/js/recrutee.js', array('jquery') );
        wp_enqueue_script( 'prodes-components', plugin_dir_url( __DIR__ ) . '/components/components.min.js', array(), '1.0' );
    }

    /**
     * Register the custom post type
     */
    public function register_post_type() {

	    $labels = array(
		    'name'                => _x( 'Recrutee vacatures', 'Post Type General Name', 'text_domain' ),
		    'singular_name'       => _x( 'Vacature', 'Post Type Singular Name', 'text_domain' ),
		    'menu_name'           => __( 'Recrutee vacatures', 'text_domain' ),
		    'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		    'all_items'           => __( 'All Items', 'text_domain' ),
		    'view_item'           => __( 'View Item', 'text_domain' ),
		    'add_new_item'        => __( 'Add New Item', 'text_domain' ),
		    'add_new'             => __( 'Add New', 'text_domain' ),
		    'edit_item'           => __( 'Edit Item', 'text_domain' ),
		    'update_item'         => __( 'Update Item', 'text_domain' ),
		    'search_items'        => __( 'Search Item', 'text_domain' ),
		    'not_found'           => __( 'Not found', 'text_domain' ),
		    'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	    );
	    $args = array(
		    'label'               => __( 'Vacatures', 'text_domain' ),
		    'description'         => __( 'Post Type voor vacatures', 'text_domain' ),
		    'labels'              => $labels,
		    'supports'            => array( 'editor', 'title', 'thumbnail', 'custom-fields', 'page-attributes', 'author' ),
		    'taxonomies'          => array( 'category', 'post_tag' ),
		    'hierarchical'        => true,
		    'public'              => true,
		    'show_ui'             => true,
		    'show_in_menu'        => true,
		    'show_in_nav_menus'   => true,
		    'show_in_admin_bar'   => true,
            'show_in_rest'        => true,
		    'menu_position'       => 5,
		    'menu_icon'           => 'dashicons-media-document',
		    'can_export'          => true,
            'rewrite' => array(
                'slug'       => 'recrutee-vacatures',
                'with_front' => true,
            ),
		    'has_archive'         => true,
		    'exclude_from_search' => false,
		    'publicly_queryable'  => true,
		    'capability_type'     => 'post',
	    );
	    register_post_type( 'recrutee-vacatures', $args );
    }

    /**
     * Call this function using the wp-rest url with a cronjob server side.
     */
    public static function import_offers_cron()
    {
        $offers = Prodes_Recrutee_Public_API::offers();

        if( $offers && ! $offers instanceof WP_Error ) array_map( array( 'Prodes_Recrutee_Offer', 'save_offer' ), $offers );
    }

    /**
     * Disables gutenberg editor
     *
     * @param       boolean      $current_status
     * @param       string       $post_type
     *
     * @return      boolean
     */
    public function disable_gutenberg($current_status, $post_type)
    {
        if ($post_type === 'recrutee-vacatures') return false;

        return $current_status;
    }
}

new Prodes_Recrutee_Init();
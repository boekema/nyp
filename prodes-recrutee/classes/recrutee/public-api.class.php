<?php
/**
 * @version 1.0
 * @author Jordy Boekema
 */
class Prodes_Recrutee_Public_API
{
    public static function offers()
    {
        $curl = new Curl();
        $curl->setOpt(CURLOPT_SSL_VERIFYPEER, FALSE);
        $curl->get('https://newyorkpizza.recruitee.com/api/offers/');

        if ($curl->error)
        {
            return new WP_Error( $curl->error_code, $curl->error_message, $curl->response );
        }
        else
        {
            $offers_recrutee = json_decode( $curl->response );
            $offers = [];

            foreach( $offers_recrutee->offers as $key => $offer )
            {
                $offers[$key] = new Prodes_Recrutee_Offer( $offer );
            }

            return $offers;
        }
    }
}
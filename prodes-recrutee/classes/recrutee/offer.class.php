<?php
/**
 * @version 1.0
 * @author Jordy Boekema
 */
class Prodes_Recrutee_Offer
{
    /**
     * Static property for GEO API
     *
     * @var mixed
     */
    static $api_request;

    /**
     * @var int
     */
    public $post_ID;

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $slug;

    /**
     * @var int
     */
    public $position;

    /**
     * @var string
     */
    public $status;

    /**
     * @var string
     */
    public $options_phone;

    /**
     * @var string
     */
    public $options_photo;

    /**
     * @var string
     */
    public $options_cover_letter;

    /**
     * @var string
     */
    public $options_cv;

    /**
     * @var string
     */
    public $remote;

    /**
     * @var string
     */
    public $country_code;

    /**
     * @var string
     */
    public $state_code;

    /**
     * @var string
     */
    public $postal_code;

    /**
     * @var int
     */
    public $min_hours;

    /**
     * @var int
     */
    public $max_hours;

    /**
     * @var array
     */
    public $open_questions;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $requirements;

    /**
     * @var string
     */
    public $location;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $country;

    /**
     * @var string
     */
    public $careers_url;

    /**
     * @var string
     */
    public $careers_apply_url;

    /**
     * @var string
     */
    public $mailbox_email;

    /**
     * @var string
     */
    public $company_name;

    /**
     * @var string
     */
    public $department;

    /**
     * @var string
     */
    public $created_at;

    /**
     * @var string
     */
    public $employment_type_code;

    /**
     * @var string
     */
    public $category_code;

    /**
     * @var string
     */
    public $experience_code;

    /**
     * @var string
     */
    public $education_code;

    /**
     * PHP 4 type contructor
     *
     * @param   array|object    $offer
     *
     * @return  void
     */
    public function __construct( $offer = null )
    {
        if( ! $offer ) return;

        if( is_array( $offer ) )
        {
            $offer = json_decode( json_encode( $offer ), false );
        }

        foreach( get_object_vars( $this ) as $key => $val )
        {
            $this->$key = $offer->$key;
        }

        $this->post_ID = $offer->ID;
    }

    public static function instance( int $offer_ID )
    {
        $post = get_post( $offer_ID );
        $post->post_ID = $offer_ID;

        return new Prodes_Recrutee_Offer( get_post( $offer_ID ) );
    }

    /**
     * Saves offer into custom post type 'vacatures'.
     *
     * @param       null|Prodes_Recrutee_Offer  $offer
     *
     * @return      void
     */
    public static function save_offer( Prodes_Recrutee_Offer $offer = null )
    {
        if( ! $offer ) return;
        if( ! function_exists( 'post_exists' ) )
        {
            require_once( ABSPATH . 'wp-admin/includes/post.php' );
        }

        if( post_exists( $offer->title, '', $offer->created_at, 'recrutee-vacatures' ) ) return;

        $metadata = [];

        /**
         * Get all data from offer and save this later as metadata.
         * We skip the values which are saved as post content.
         */
        foreach( get_object_vars( $offer ) as $key => $val )
        {
            if( $key === 'id' || $key === 'description' || $key === 'title' || $key === 'created_at' ) continue;

            $metadata[$key] = $val;
        }

        $metadata['lat'] = self::get_geometry( $offer->city )->lat;
        $metadata['lng'] = self::get_geometry( $offer->city )->lng;

        wp_insert_post( array(
            'post_content'  => $offer->description,
            'post_title'    => $offer->title,
            'post_type'     => 'recrutee-vacatures',
            'post_date'     => $offer->created_at,
            'meta_input'    => $metadata
        ) );
    }

    /**
     * Get geometry data using Google GEO API
     *
     * @param   string $address
     *
     * @return  object|array|null
     */
    public static function get_geometry( string $address )
    {
        /**
         * Only run once per request
         */
        if( ! self::$api_request )
        {
            self::$api_request = json_decode( file_get_contents( 'https://maps.googleapis.com/maps/api/geocode/json?address='. $address .'&key=AIzaSyCoHw7vacZlwZI-NlKVwIEho8Psr5DWgSs' ) );
        }

        if( self::$api_request->status === 'OK' )
        {
            if( isset( self::$api_request->results ) )
            {
                return self::$api_request->results[0] ? self::$api_request->results[0]->geometry->location : null;
            }
        }

        return self::$api_request;
    }
}
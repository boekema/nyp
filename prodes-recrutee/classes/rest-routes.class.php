<?php
/**
 * @version v1.0.0
 * @author Jordy Boekema
 */
class Prodes_Recrutee_REST_Routes
{
    public function __construct()
    {
        add_action( 'rest_api_init', array( &$this, 'register_rest_routes' ) );
    }

    public function register_rest_routes()
    {
        /**
         * Route to return a list of offers
         */
        register_rest_route( 'prodes-recrutee/v1', '/public/offers', array(
            'methods' => 'GET',
            'callback' => array( 'Prodes_Recrutee_REST_Callbacks', 'get_offers' )
        ) );

        /**
         * Import orders cron
         */
        register_rest_route( 'prodes-recrutee/v1', '/public/offers/import', array(
            'methods' => 'GET',
            'callback' => array( 'Prodes_Recrutee_REST_Callbacks', 'import_offers' )
        ) );

        /**
         * Apply for a job offer
         */
        register_rest_route( 'prodes-recrutee/v1', '/public/offers(?:/(?P<offer_slug>.*))?/candidates', array(
            'methods' => 'POST',
            'callback' => array( 'Prodes_Recrutee_REST_Callbacks', 'apply_for_offer' )
        ) );
    }
}

new Prodes_Recrutee_REST_Routes();
<?php
/**
 * Plugin Name: Prodes - New York Pizza API
 * Plugin URI: https://prodes.nl
 * Description: New York Pizza API
 * Version: 1.0.0
 * Author: Jordy Boekema
 **/

if ( ! defined( 'ABSPATH' ) )
{
	exit; // Exit if accessed directly
}

require_once __DIR__ . '/classes/nyp/offer.class.php';
require_once __DIR__ . '/classes/api.class.php';
require_once __DIR__ . '/classes/rest-callbacks.class.php';
require_once __DIR__ . '/classes/rest-routes.class.php';
require_once __DIR__ . '/classes/init.class.php';
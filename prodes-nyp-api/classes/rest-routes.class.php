<?php
/**
 * @version v1.0.0
 * @author Jordy Boekema
 */
class Prodes_NYP_API_REST_Routes
{
    public function __construct()
    {
        add_action( 'rest_api_init', array( &$this, 'register_rest_routes' ) );
    }

    public function register_rest_routes()
    {
        /**
         * Route to return a list of vacancies
         */
        register_rest_route( 'prodes-nyp-api/v1', '/store/vacancies', array(
            'methods' => 'GET',
            'callback' => array( 'Prodes_NYP_API_REST_Callbacks', 'get_vacancies' )
        ) );
    }
}

new Prodes_NYP_API_REST_Routes();
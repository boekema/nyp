<?php
/**
 * @version v1.0.0
 * @author Jordy Boekema
 */
class Prodes_NYP_API_REST_Callbacks
{
    public static function get_vacancies()
    {
        return Prodes_NYP_API::get_vacancies();
    }
}
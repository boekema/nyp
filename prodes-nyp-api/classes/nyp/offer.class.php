<?php
/**
 * @version 1.0
 * @author Jordy Boekema
 */
class Prodes_NYP_API_Offer
{
    /**
     * Static property for GEO API
     *
     * @var mixed
     */
    static $api_request;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $employment;

    /**
     * @var string
     */
    public $publicationDate;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $storeName;

    /**
     * @var int
     */
    public $storeId;

    /**
     * @var string
     */
    public $contactName;

    /**
     * @var string
     */
    public $contactPhoneNumber;

    /**
     * @var string
     */
    public $contactEmailAddress;

    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $requirements;

    /**
     * @var string
     */
    public $whatWeOffer;

    /**
     * PHP4 Type constructor
     *
     * @param       mixed       $offer
     *
     * @return      void
     */
    public function __construct( $offer = null )
    {
        if( ! $offer )
        {
            return;
        }

        foreach( get_object_vars( $this ) as $key => $value )
        {
            $this->$key = $offer->$key;
        }
    }

    /**
     * Adds new offer to NYP vacatures posts in WordPress.
     *
     * @param   null|stdClass $offer
     *
     * @return  null|Prodes_NYP_API_Offer
     */
    public static function add_offer( stdClass $offer = null )
    {
        if( ! $offer )
        {
            $offer = new self( $offer );
        }

        if( ! $offer ) return;
        if( ! function_exists( 'post_exists' ) )
        {
            require_once( ABSPATH . 'wp-admin/includes/post.php' );
        }

        if( post_exists( $offer->name, '', '', 'nyp-vacatures' ) ) return;

        $metadata = [];

        /**
         * Get all data from offer and save this later as metadata.
         * We skip the values which are saved as post content.
         */
        foreach( get_object_vars( $offer ) as $key => $val )
        {
            if( $key === 'name' || $key === 'description' ) continue;

            $metadata[$key] = $val;
        }

        $metadata['lat'] = self::get_geometry( $offer->city )->lat;
        $metadata['lng'] = self::get_geometry( $offer->city )->lng;

        wp_insert_post( array(
            'post_content'  => $offer->description,
            'post_title'    => $offer->name,
            'post_type'     => 'nyp-vacatures',
            'post_date'     => $offer->created_at,
            'meta_input'    => $metadata
        ) );

        return $offer;
    }

    /**
     * Get geometry data using Google GEO API
     *
     * @param   string $address
     *
     * @return  object|array|null
     */
    public static function get_geometry( string $address )
    {
        /**
         * Only run once per request
         */
        if( ! self::$api_request )
        {
            self::$api_request = json_decode( file_get_contents( 'https://maps.googleapis.com/maps/api/geocode/json?address='. $address .'&key=AIzaSyCoHw7vacZlwZI-NlKVwIEho8Psr5DWgSs' ) );
        }

        if( self::$api_request->status === 'OK' )
        {
            if( isset( self::$api_request->results ) )
            {
                return self::$api_request->results[0] ? self::$api_request->results[0]->geometry->location : null;
            }
        }

        return self::$api_request;
    }
}
<?php
/**
 * @version 1.0
 * @author Jordy Boekema
 */
class Prodes_NYP_API
{
    public $api_url = 'http://prelive-restapi.newyorkpizza.about-blank.nl/api/v1/';

    protected $api_key = 'd695e5db-2784-4934-8b62-d8debfe6f86f';

    public function __construct()
    {

    }

    public static function get_vacancies()
    {
        $prodes_nyp_api = new Prodes_NYP_API();

        $curl = new Curl();
        $curl->setOpt(CURLOPT_SSL_VERIFYPEER, FALSE);
        $curl->setHeader('apiKey', $prodes_nyp_api->api_key);
        $curl->get( $prodes_nyp_api->api_url . 'store/vacancies' );

        $response = json_decode($curl->response);

        foreach( $response as $offer )
        {
            Prodes_NYP_API_Offer::add_offer( $offer );
            break;
        }
    }
}
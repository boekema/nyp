<?php
/**
 * @version v1.0.0
 * @author Jordy Boekema
 */
class Prodes_Upload_API_Callbacks
{
    public static function upload( WP_REST_Request $request )
    {
        return Prodes_Upload::upload( $_FILES['file'], $request->get_param('upload_sub_dir') );
    }
}
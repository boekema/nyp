<?php
/**
 * @version v1.0.0
 * @author Jordy Boekema
 */
class Prodes_Upload_API
{
    public function __construct()
    {
        add_action( 'rest_api_init', array( &$this, 'register_rest_routes' ) );
    }

    public function register_rest_routes()
    {
        register_rest_route( 'prodes-upload', '/v1/upload/', array(
            'methods' => 'POST',
            'callback' => array( 'Prodes_Upload_API_Callbacks', 'upload' )
        ) );
    }
}

new Prodes_Upload_API();
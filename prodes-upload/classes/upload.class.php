<?php
/**
 * @version v1.0.0
 * @author Jordy Boekema
 */
class Prodes_Upload
{
    /**
     * Array with file contents
     *
     * @var object
     */
    protected $file;

    /**
     * Upload root directory
     *
     * @var string
     */
    protected $upload_dir = '/uploads/prodes';

    /**
     * Max file size, default is 500000 bytes
     *
     * @var int
     */
    protected $max_file_size = 500000;

    /**
     * Last inserted MySQL recurd
     *
     * @var int
     */
    protected $last_inserted_ID;

    /**
     * Returns file URL
     *
     * @var string
     */
    public $output;

    /**
     * Absolute path to file
     *
     * @var string
     */
    public $file_location;

    /**
     * Upload a file to the system. File will be stored in '/wp-content/uploads/prodes/{$upload_sub_dir}/file.ext'
     *
     * @param   array   $file               - File array
     * @param   string  $upload_sub_dir     - (optional) If defined, file will be stored in an additional subdirectory
     */
    private function __construct( $file = null, string $upload_sub_dir = '' )
    {
        if( ! $file )
        {
            return;
        }

        global $wpdb;

        $this->file = (object) $file;
        $this->upload_dir = $this->upload_dir . DIRECTORY_SEPARATOR . $upload_sub_dir . DIRECTORY_SEPARATOR;

        $this->create_directories();

        /**
         * Get max file size set in php.ini
         */
        $max_upload = min(ini_get('post_max_size'), ini_get('upload_max_filesize'));
        $max_upload = str_replace('M', '', $max_upload);
        $max_upload = $max_upload * 1024 * 1024; // size in bytes

        $this->max_file_size = $max_upload;

        /**
         * Check if file size is within PHP limits
         */
        if( ! $this->is_valid() )
            throw new ErrorException('File is to big');

        if( ! $this->put_contents() )
            throw new ErrorException('Couldn\'t upload the file.');

        $this->output = str_replace('\\', '/', content_url() .  $this->upload_dir . DIRECTORY_SEPARATOR . $this->file->filename);
        $this->output = str_replace('///', '/', $this->output);
    }

    /**
     * Creates missing directories
     */
    private function create_directories()
    {
        $directory = WP_CONTENT_DIR;

        foreach( explode( DIRECTORY_SEPARATOR, $this->upload_dir ) as $dir )
        {
            $directory .= DIRECTORY_SEPARATOR . $dir;

            if( ! file_exists( $directory ) )
            {
                mkdir( $directory );
            }
        }
    }

    /**
     * Check if file is valid size
     *
     * @return boolean
     */
    private function is_valid()
    {
        return $this->file->size <= $this->max_file_size;
    }

    /**
     * Puts file on the system, returns true when succeeded
     *
     * @return boolean
     */
    private function put_contents()
    {
        $this->file_location = WP_CONTENT_DIR . $this->upload_dir . DIRECTORY_SEPARATOR . $this->file->filename;

        /**
         * If failed to move file using the default upload method, try to use it manualy.
         */
        if( ! move_uploaded_file( $this->file->tmp_name, $this->file_location ))
        {
            if( rename( $this->file->tmp_name, $this->file_location ) )
            {
                return true;
            }

            return false;
        }

        return true;
    }

    /**
     * Upload a file to the system. File will be stored in '/wp-content/uploads/$upload_sub_dir/Microsoft_Profile::id/file.ext'
     *
     * @param   array   $file               - File array
     * @param   string  $upload_sub_dir     - (optional) If defined, file will be stored in an additional subdirectory
     *
     * @return Prodes_Upload|WP_Error
     */
    public static function upload( $file = null, string $upload_sub_dir = '' )
    {
        if( ! $file )
        {
            return new WP_Error(500, 'File is undefined.');
        }

        /**
         * Add few extra parameters for the file
         */
        $file['filename'] = bin2hex(openssl_random_pseudo_bytes(32)) . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);

        return new Prodes_Upload( $file, $upload_sub_dir );
    }
}
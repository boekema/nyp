<?php
/**
 * Plugin Name: Prodes - Upload
 * Plugin URI: https://prodes.nl
 * Description: Adds upload class to a prodes website
 * Version: 1.0.0
 * Author: Jordy Boekema
 **/

if ( ! defined( 'ABSPATH' ) )
{
	exit; // Exit if accessed directly
}

require_once __DIR__ . '/classes/init.class.php';
require_once __DIR__ . '/classes/upload.class.php';
require_once __DIR__ . '/classes/api.callbacks.class.php';
require_once __DIR__ . '/classes/api.class.php';